import { select } from 'd3-selection';
import { transition } from 'd3-transition';

let chart = select('.covid-svg'),
    cuadrados = new Array(900),
    shuffleCuadrados = new Array(900),
    tamano = window.innerWidth < 768 ? 6 : 7,
    radio = window.innerWidth < 768 ? 3 : 3.5, 
    primerInfectado = document.getElementsByClassName('first-infected')[0];

//Colores
let grisInicial = '#dec7c7', 
    enfermos = '#e34443', 
    sanos = '#dec7c7', 
    inmunizados = '#94BAD1', 
    vacunados = '#101931';

export function initScrollCharts() {
    //Primero damos el mismo alto para obtener un cuadrado
    chart.style('height', chart.style('width'));

    //Espacio restante
    let espacioRestante = parseInt(chart.style('width')) - (30 * tamano);
    let espacioItem = espacioRestante / 29;

    cuadrados = setData();
    shuffleCuadrados = shuffle(cuadrados);

    //Luego creamos 900 cuadrados en su interior y les otorgamos todos los estados
    chart.selectAll('rect')
        .data(cuadrados)
        .enter()
        .append('rect')
        .style('width', tamano + 'px')
        .style('height', tamano + 'px')
        .attr('rx', radio)
        .attr('ry', radio)
        .style('fill', grisInicial)
        .attr('y', function(d, i) {
            let fila = Math.floor(i / 30);
            return fila * (tamano + espacioItem);
        })
        .attr('x', function(d, i) {
            let columna = Math.ceil(i % 30);            
            return columna * (tamano + espacioItem);
        })
        .attr('class', 'cuadrados')
        .attr('id', function(d,i) {
            return `cuadrado-${i}`;
        });

    //Lógica para poner uno en concreto infectado y hacer otros invisible
    let alto = (parseInt(chart.style('height')) / 2) - 14 - tamano - (window.innerWidth < 470 ? 0.1 : 1.5);
    document.getElementsByClassName('first-infected')[0].style.top = alto + 'px';
}

export function setScrollChart(scrollIdx) {
    if(scrollIdx == 0) {
        setZero()
    } else {
        if (!primerInfectado.classList.contains('hidden')){
            primerInfectado.classList.add('hidden');
        }
        setScenario(scrollIdx);
    }
}

function setZero() {
    //Texto
    if (primerInfectado.classList.contains('hidden')){
        primerInfectado.classList.remove('hidden');
    }
        
    //Todos los demás con el color inicial
    chart.selectAll('.cuadrados').style('fill', grisInicial);
    chart.select('#cuadrado-435').style('fill', enfermos); 
}

function setScenario(idx) {
    if (idx == 1) {
        chart.selectAll('.cuadrados')
            .data(cuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.firstInitial == 'sano') {
                    return sanos;
                } else if (d.firstInitial == 'enfermo') {
                    return enfermos;
                }
            });
    } else if (idx == 2) {
        chart.selectAll('.cuadrados')
            .data(shuffleCuadrados)
            .transition()
            .duration(900)
            .delay(function(d, i) {return i;})
            .style('fill', function(d,i) {
                if(d.firstIntermediate == 'sano') {
                    return sanos;
                } else if (d.firstIntermediate == 'enfermo') {
                    return enfermos;
                }
            });
    } else if (idx == 3) {
        chart.selectAll('.cuadrados')
            .data(cuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.firstFinal == 'sano') {
                    return sanos;
                } else if (d.firstFinal == 'enfermo') {
                    return enfermos;
                }
            });
    } else if (idx == 4) {
        chart.selectAll('.cuadrados')
            .data(cuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.secondInitial == 'sano') {
                    return sanos;
                } else if (d.secondInitial == 'enfermo') {
                    return enfermos;
                } else if (d.secondInitial == 'inmunizado') {
                    return inmunizados;
                } else {
                    return vacunados;
                }
            });
    } else if (idx == 5) {
        chart.selectAll('.cuadrados')
            .data(cuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.secondFinal == 'sano') {
                    return sanos;
                } else if (d.secondFinal == 'enfermo') {
                    return enfermos;
                } else if (d.secondFinal == 'inmunizado') {
                    return inmunizados;
                } else {
                    return vacunados;
                }
            });
    } else if (idx == 6) {
        chart.selectAll('.cuadrados')
            .data(cuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.thirdInitial == 'sano') {
                    return sanos;
                } else if (d.thirdInitial == 'enfermo') {
                    return enfermos;
                } else if (d.thirdInitial == 'inmunizado') {
                    return inmunizados;
                } else {
                    return vacunados;
                }
            });
    } else if (idx == 7) {
        chart.selectAll('.cuadrados')
            .data(cuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.thirdFinal == 'sano') {
                    return sanos;
                } else if (d.thirdFinal == 'enfermo') {
                    return enfermos;
                } else if (d.thirdFinal == 'inmunizado') {
                    return inmunizados;
                } else {
                    return vacunados;
                }
            });
    } else if (idx == 8) {
        chart.selectAll('.cuadrados')
            .data(shuffleCuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.fourthInitial == 'sano') {
                    return sanos;
                } else if (d.fourthInitial == 'enfermo') {
                    return enfermos;
                } else if (d.fourthInitial == 'inmunizado') {
                    return inmunizados;
                } else {
                    return vacunados;
                }
            });
    } else if (idx == 9) {
        chart.selectAll('.cuadrados')
            .data(shuffleCuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.fourthFinal == 'sano') {
                    return sanos;
                } else if (d.fourthFinal == 'enfermo') {
                    return enfermos;
                } else if (d.fourthFinal == 'inmunizado') {
                    return inmunizados;
                } else {
                    return vacunados;
                }
            });
    } else if (idx == 10) {
        chart.selectAll('.cuadrados')
            .data(shuffleCuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.fifthInitial == 'sano') {
                    return sanos;
                } else if (d.fifthInitial == 'enfermo') {
                    return enfermos;
                } else if (d.fifthInitial == 'inmunizado') {
                    return inmunizados;
                } else {
                    return vacunados;
                }
            });
    } else if (idx == 11) {
        chart.selectAll('.cuadrados')
            .data(shuffleCuadrados)
            .transition()
            .duration(900)
            .delay(function(d,i) {return i;})
            .style('fill', function(d,i) {
                if(d.fifthFinal == 'sano') {
                    return sanos;
                } else if (d.fifthFinal == 'enfermo') {
                    return enfermos;
                } else if (d.fifthFinal == 'inmunizado') {
                    return inmunizados;
                } else {
                    return vacunados;
                }
            });
    }
}

function setData() {
    let aux = [];
    let countEnfermosInventado = 0;
    let countInmunizados = 0, countInmunizados2 = 0;
    let countEnfermosThird = 0, countSanosInmunizadosThird = 0, countSanosVacunadosThird = 0;
    for (let i = 0; i < 900; i++) {
        let auxObj = {};

        //Primer escenario > Estado inicial
        if (i == 130 || i == 435 || i == 713) {
            auxObj.firstInitial = 'enfermo';
        } else {
            auxObj.firstInitial = 'sano';
        }

        //Primer escenario > Estado intermedio
        let randomIntermedio = Math.random();
        if (randomIntermedio < 0.5 && countEnfermosInventado < 600){
            auxObj.firstIntermediate = 'enfermo';
            countEnfermosInventado++;
        } else {
            auxObj.firstIntermediate = 'sano';
        }

        //Primer escenario > Estado final
        if (i == 130 || i == 130 - 33 || i == 130 - 32 || i == 130 - 31 || i == 130 - 30 || i == 130 - 1 || i == 130 - 2 || i == 130 + 1 || i == 130 + 2 || i == 130 + 28 || i == 130 + 29 || i == 130 + 30 || i == 130 + 31 || i == 130 + 32 || i == 130 + 33 ||
            i == 435 || i == 435 - 62 || i == 435 - 61 || i == 435 - 60 || i == 435 - 31 || i == 435 - 30 || i == 435 - 29 || i == 435 - 2 || i == 435 - 1 || i == 435 + 1 || i == 435 + 29 || i == 435 + 30 || i == 435 + 31 || i == 435 + 59 || i == 435 + 60 ||
            i == 713 || i == 713 - 35 || i == 713 - 34 || i == 713 - 33 || i == 713 - 32 || i == 713 - 31 || i == 713 - 30 || i == 713 - 29 || i == 713 - 28 || i == 713 - 27 || i == 713 - 4 || i == 713 - 3 || i == 713 - 2 || i == 713 - 1 || i == 713 + 1) {
                auxObj.firstFinal = 'enfermo';  
        } else {
            auxObj.firstFinal = 'sano';
        }

        //Segundo escenario > Estado inicial y final
        if (i == 130 || i == 130 - 33 || i == 130 - 32 || i == 130 - 31 || i == 130 - 30 || i == 130 - 1 || i == 130 - 2 || i == 130 + 1 || i == 130 + 2 || i == 130 + 28 || i == 130 + 29 || i == 130 + 30 || i == 130 + 31 || i == 130 + 32 || i == 130 + 33 ||
            i == 435 || i == 435 - 62 || i == 435 - 61 || i == 435 - 60 || i == 435 - 31 || i == 435 - 30 || i == 435 - 29 || i == 435 - 2 || i == 435 - 1 || i == 435 + 1 || i == 435 + 29 || i == 435 + 30 || i == 435 + 31 || i == 435 + 59 || i == 435 + 60 ||
            i == 713 || i == 713 - 35 || i == 713 - 34 || i == 713 - 33 || i == 713 - 32 || i == 713 - 31 || i == 713 - 30 || i == 713 - 29 || i == 713 - 28 || i == 713 - 27 || i == 713 - 4 || i == 713 - 3 || i == 713 - 2 || i == 713 - 1 || i == 713 + 1 ||
            i == 726 || i == 726 - 33 || i == 726 - 32 || i == 726 - 31 || i == 726 - 30 || i == 726 - 1 || i == 726 - 2 || i == 726 + 1 || i == 726 + 2 || i == 726 + 28 || i == 726 + 29 || i == 726 + 30 || i == 726 + 59 || i == 726 + 60 || i == 130 + 33 ||
            i == 173 || i == 173 - 62 || i == 173 - 61 || i == 173 - 60 || i == 173 - 31 || i == 173 - 30 || i == 173 - 29 || i == 173 - 3 || i == 173 - 2 || i == 173 - 1 || i == 173 + 29 || i == 173 + 30 || i == 173 + 31 || i == 173 + 59 || i == 173 + 60 ||
            i == 303 || i == 303 - 32 || i == 303 - 60 || i == 303 - 31 || i == 303 - 30 || i == 303 - 29 || i == 303 - 28 || i == 130 - 1 || i == 303 - 2 || i == 303 + 1 || i == 303 + 2 || i == 303 + 28 || i == 303 + 29 || i == 303 + 30 || i == 303 + 31 || i == 303 + 32 || i == 303 + 33) {
                auxObj.secondInitial = 'enfermo';
                
                if(i == 130 || i == 130 - 33 || i == 130 - 32 || i == 130 - 31 || i == 130 - 30 || i == 130 - 1 ||
                    i == 435 || i == 435 - 62 || i == 435 - 61 || i == 435 - 60 || i == 435 - 31 || i == 435 - 30 ||
                    i == 713 - 30 || i == 713 - 29 || i == 713 - 28 || i == 713 - 27 || i == 713 - 4 || i == 713 - 3 || i == 713 - 2 || i == 713 - 1 || i == 713 + 1 ||
                    i == 303 - 29 || i == 303 - 28 || i == 130 - 1 || i == 303 - 2 || i == 303 + 1 || i == 303 + 2 || i == 303 + 28 || i == 303 + 29 || i == 303 + 30 || i == 303 + 31 || i == 303 + 32 || i == 303 + 33 ||
                    i == 726 || i == 726 - 33 || i == 726 - 32 || i == 726 - 31 || i == 726 - 30 || i == 726 - 1 || i == 726 - 2 || i == 726 + 1 || i == 726 + 2 || i == 726 + 28 || i == 726 + 29 || i == 726 + 30 || i == 726 + 59 ||
                    i == 173 || i == 173 - 62 || i == 173 - 61 || i == 173 - 60 || i == 173 - 30){
                        auxObj.secondFinal = 'inmunizado';
                } else {
                    auxObj.secondFinal = 'enfermo';
                }
        } else {
            let random = Math.random();
            if (random < 0.1 && countInmunizados < 45) {
                auxObj.secondInitial = 'inmunizado';
                auxObj.secondFinal = 'inmunizado';
                countInmunizados++;
            } else {
                auxObj.secondInitial = 'sano';
            }

            if(auxObj.secondInitial == 'sano') {
                let random = Math.random();
                if(random < 0.1 && countInmunizados2 < 45) {
                    auxObj.secondFinal = 'inmunizado';
                    countInmunizados2++;
                } else {
                    auxObj.secondFinal = 'sano';
                }
            }
        }

        //Tercer escenario > Estado inicial y final
        if (auxObj.secondFinal == 'enfermo') {
            let random = Math.random();
            if ((random > 0.4 && random < 0.6) && countEnfermosThird < 27) {
                auxObj.thirdInitial = 'enfermo';
                auxObj.thirdFinal = 'enfermo';
                countEnfermosThird++;
            } else {
                auxObj.thirdInitial = 'enfermo';
                auxObj.thirdFinal = 'inmunizado';
            }
        } else if (auxObj.secondFinal == 'sano') {
            let random = Math.random();

            if (random < 0.15 && countSanosVacunadosThird < 18) {
                auxObj.thirdInitial = 'sano';
                auxObj.thirdFinal = 'vacunado';
                countSanosVacunadosThird++;
            } else if (random < 0.2 && countSanosInmunizadosThird < 27) {
                auxObj.thirdInitial = 'sano';
                auxObj.thirdFinal = 'inmunizado';
                countSanosInmunizadosThird++;
            } else {
                auxObj.thirdInitial = 'sano';
                auxObj.thirdFinal = 'sano';
            }

        } else {
            auxObj.thirdInitial = 'inmunizado';
            auxObj.thirdFinal = 'inmunizado';
        }

        //Cuarto escenario
        if (i < 27) {
            if (i < 9) {
                auxObj.fourthInitial = 'enfermo';
                auxObj.fourthFinal = 'enfermo';
            } else {
                auxObj.fourthInitial = 'enfermo';
                auxObj.fourthFinal = 'vacunado';
            }
        } else if (i >= 27 && i < 207) {
            if (i < 117) {
                auxObj.fourthInitial = 'inmunizado';
                auxObj.fourthFinal = 'inmunizado';
            } else {
                auxObj.fourthInitial = 'inmunizado';
                auxObj.fourthFinal = 'vacunado';
            }
        } else if (i >= 207 && i < 765) {
            if (i < 279) {
                auxObj.fourthInitial = 'sano';
                auxObj.fourthFinal = 'vacunado';
            } else {
                auxObj.fourthInitial = 'sano';
                auxObj.fourthFinal = 'sano';
            }
        } else {
            auxObj.fourthInitial = 'vacunado';
            auxObj.fourthFinal = 'vacunado';
        }

        //Quinto escenario
        if(i < 18) {
            if (i < 9) {
                auxObj.fifthInitial = 'enfermo';
                auxObj.fifthFinal = 'enfermo';
            } else {
                auxObj.fifthInitial = 'enfermo';
                auxObj.fifthFinal = 'vacunado';
            }
        } else if (i >= 18 && i < 243) {
            if (i < 45) {
                auxObj.fifthInitial = 'sano';
                auxObj.fifthFinal = 'vacunado';
            } else {
                auxObj.fifthInitial = 'sano';
                auxObj.fifthFinal = 'sano';
            }
        } else if (i >= 243 && i < 315) {
            if (i < 252) {
                auxObj.fifthInitial = 'inmunizado';
                auxObj.fifthFinal = 'vacunado';
            } else {
                auxObj.fifthInitial = 'inmunizado';
                auxObj.fifthFinal = 'inmunizado';
            }
        } else {
            auxObj.fifthInitial = 'vacunado';
            auxObj.fifthFinal = 'vacunado';
        }

        aux.push(auxObj);
    }

    return aux;
}

function shuffle(array) {
    let aux = [...array];
    let currentIndex = aux.length, randomIndex;

    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        [aux[currentIndex], aux[randomIndex]] = [aux[randomIndex], aux[currentIndex]];
    }

    return aux;
}