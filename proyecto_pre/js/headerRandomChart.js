import { select } from 'd3-selection';
import { range } from 'd3-array';
import { forceSimulation, forceCollide, forceManyBody, forceCenter } from 'd3-force';

let angle, offset, data, root, color, 
    canvasContainer,
    number = window.innerWidth <= 768 ? 450 : 900,
    iterations = 42,
    target = 0.0275,
    velocity = 0.035,
    size = 2.85,
    radius = 2.25,
    dispersion = -15;

export function drawHeaderRandomChart() {
    let width = document.getElementById('header-chart').clientWidth,
        height = document.getElementById('header-chart').clientHeight;
    
    canvasContainer = select('#header-chart').append("canvas")
        .attr("width", width)
        .attr("height", height);

    color = ['#A1C5DB', '#101931', '#fbf8f6', '#e34443', '#A1C5DB'];

    data = range(number).map(function(i) {
        angle = Math.random() * Math.PI * 2;
        offset = size + radius + dispersion;
        return {
          x: offset + Math.cos(angle) * radius + rand(-dispersion, dispersion),
          y: offset + Math.sin(angle) * radius + rand(-dispersion, dispersion),
          radius: radius,
          colored: color[i % 6]
        };      
    });

    root = data[0],
    root.radius = 0;
    root.fixed = true;

    root.px = width / 2;
    root.py = height / 2;

    let context = canvasContainer.node().getContext("2d");

    let simulation = forceSimulation()
        .alphaTarget(target) // stay hot
        .velocityDecay(velocity) // low friction
        .force("center", forceCenter(document.getElementById('header-chart').clientWidth / 2, document.getElementById('header-chart').clientHeight / 2))
        .force("collide", forceCollide().radius(d => d.radius).iterations(iterations))
        .force("charge", forceManyBody().strength(0.015));

    simulation
        .nodes(data)
        .on('tick', function() {
            context.clearRect(0, 0, width, height);
            context.beginPath();

            for (const node of data) {
                context.beginPath();

                context.moveTo(node.x, node.y);
                context.arc(node.x, node.y, 2.85, 0, 2 * Math.PI);

                context.fillStyle = node.colored;
                context.fill();
            }
        });

    setInterval(() => { //Parar el requestAnimationFrame cuando hayan pasado 30 segundos
        simulation.stop();
    }, 30000);
}

//Helpers
function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}