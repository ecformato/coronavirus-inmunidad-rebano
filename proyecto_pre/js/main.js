import { setRRSSLinks, scrollActiv } from '../../common_projects_utilities/js/pure-branded-helpers';
import { isElementInViewport, isElementInViewportCards, percentageOfElement } from '../../common_projects_utilities/js/dom-helpers';
import { setAvailableHeight } from './helpers';
import { drawHeaderRandomChart } from './headerRandomChart';
import { initScrollCharts, setScrollChart } from './scrollCharts';

import "intersection-observer";
import scrollama from "scrollama";
import { select } from 'd3-selection'; 

//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

/////////////
//Elementos para SCROLLAMA + D3
let authorsDesktop = document.getElementsByClassName('credits--desktop')[0];
let ownScroll = document.getElementById('scrolly');
let scrolly = select('#scrolly');
let figure = scrolly.select('figure');
let article = scrolly.select('article');
let step = article.selectAll('.step');
let cards = document.getElementsByClassName('card');
let stickyElement = document.querySelector('.sticky-viz');
let isScrollyVisible = false;

//Ejecución inicial
setAvailableHeight();
setRRSSLinks();
drawHeaderRandomChart();
initScrollCharts();

// Funciones asociadas a otros eventos del DOM
window.addEventListener('resize', () => {
    setAvailableHeight();
});

//////// Otros ////////
let viewportHeight = window.innerHeight;
let logoFooter = document.getElementById("logoFooter");
let sharing = document.getElementById("sharing");
let arrow = document.getElementById('scroll-arrow');
let summaries = document.getElementsByClassName('content__summary');

let sumupBig = document.getElementsByClassName('sumup__big')[0];
let bigTopRects = sumupBig.getElementsByClassName('top-rect');
let sumupSmall = document.getElementsByClassName('sumup__small')[0];
let smallTopRects = sumupSmall.getElementsByClassName('top-rect');

arrow.addEventListener('click', function() {
    let contenido = document.getElementsByClassName('container')[0].getBoundingClientRect();
    window.scrollTo({top: contenido.top + (window.innerWidth < 972 ? 20 : 140), behavior: 'smooth'});
});

window.addEventListener('scroll', function(e){
	scrollActiv();

    //ESCENARIOS RESUMEN
    if(isElementInViewport(sumupBig)){
        for(let i = 0; i < bigTopRects.length; i++) {
            bigTopRects[i].classList.add('visible');
        }
    }

    if(isElementInViewport(sumupSmall)){
        for(let i = 0; i < smallTopRects.length; i++) {
            smallTopRects[i].classList.add('visible');
        }
    }
	
    //ASIDE
	let scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
	if ( scrollTop > viewportHeight ){
		sharing.classList.add("visible")
	} else {
		sharing.classList.remove("visible")
	}

	if (window.innerWidth < 993) {
		if (isElementInViewport(logoFooter)) {
			sharing.classList.remove("visible")
		}
	}

    //AUTORES (SÓLO DESKTOP)
    if(window.innerWidth > 972) {
        if(isElementInViewport(authorsDesktop)){
            document.getElementById('credit-line').style.height = '100%';
        }        
    }

    //WORKAROUND PARA CARDS
    isScrollVisible();

    //SUMARIOS
	for (let i = 0; i < summaries.length; i++) {
		if(isElementInViewport(summaries[i])) {
			if(!summaries[i].classList.contains('is-visible')) {
				summaries[i].classList.add('is-visible')
			}
		}
	}

    //VISIBILIDAD DE CARDS
    for (let i = 0; i < cards.length; i++) {
        if(isElementInViewportCards(cards[i])) {
            cards[i].style.opacity = '0.95';
        } else {
            cards[i].style.opacity = '0.5';
        }
    }
});

function isScrollVisible() {
    let topScroll = ownScroll.getBoundingClientRect().top + window.pageYOffset;
    let bottomScroll = ownScroll.getBoundingClientRect().top + ownScroll.getBoundingClientRect().height + window.pageYOffset - (viewportHeight / 1.05);
    let scroll = window.pageYOffset;
    let ownHeight = ownScroll.getBoundingClientRect().y;
    
    if(ownHeight < 60 && scroll < bottomScroll - 60) {
        if(isScrollyVisible == false) {           
            if(window.innerWidth >= 993) {
                let prueba = stickyElement.getBoundingClientRect().width;
                let distancia = (document.body.offsetWidth - 972) / 2;
                console.log(window.innerWidth, distancia);
                console.log(document.body.offsetWidth);
                stickyElement.style.left = `${distancia}px`;
                stickyElement.style.width = prueba + 'px';
            }

            //Hacemos activa la clase
            stickyElement.classList.add('active');
            isScrollyVisible = true;
        }
    } else {
        isScrollyVisible = false;
        stickyElement.classList.remove('active');
        if(window.innerWidth >= 993) {
            stickyElement.style.left = '0px';
        }        
        
        if (scroll < topScroll) {
            stickyElement.style.top = '0px';
            stickyElement.style.bottom = 'auto';
        } else {
            stickyElement.style.top = 'auto';
            stickyElement.style.bottom = '0px';
        }        
    }  
}

//// Scrollama
const scroller = scrollama();
init();

function init() {
    handleResize();

    scroller
        .setup({
            step: "#scrolly article .step",
            offset: 0.75,
            debug: false
        })
        .onStepEnter((response) => {
            setScrollChart(response.index);
        });

    // setup resize event
    window.addEventListener("resize", handleResize);
}

function handleResize() {    
    // Altura de los pasos
    let stepH = Math.floor(window.innerHeight * (window.innerWidth < 993 ? 1.5 : 1));
    step.style("height", stepH + "px");
    
    // Altura de la visualización >> Modificar si no cuadra bien cuando metamos la real
    let figureHeight = window.innerHeight;
    let figureMarginTop = 0;
    figure.style("height", figureHeight + "px").style("top", figureMarginTop + "px");

    scroller.resize();
}