let viewportHeight = window.innerHeight;
let viewportWidth = window.innerWidth;

function isMobile() {
	return viewportWidth < 768;
}

function isSmallMobile() {
	return viewportWidth < 525;
}

function isDevice() { //Mejorar nombre
    return viewportWidth < 993;
}

function isElementInViewport(el) {
    var scroll = window.scrollY || window.pageYOffset
    var boundsTop = el.getBoundingClientRect().top + scroll
    
    var viewport = {
        top: scroll,
        bottom: scroll + viewportHeight,
    }
    
    var bounds = {
        top: Math.floor(boundsTop) + el.offsetHeight,
        bottom: boundsTop + el.clientHeight,
    }
    

    return ( (viewport.bottom > bounds.top) && (viewport.top < bounds.bottom) ) 
}

function isElementInViewportCards(el) {
    let scroll = window.scrollY || window.pageYOffset;
    let viewportCuartil = viewportHeight / 4;
    let boundsTop = el.getBoundingClientRect().top + scroll + viewportCuartil;
    
    var viewport = {
        top: scroll,
        bottom: scroll + viewportHeight,
    }

    return viewport.bottom > boundsTop;
}

function percentageOfElement(el){
    // Get the relevant measurements and positions
    const viewportHeight = window.innerHeight;
    const scrollTop = window.scrollY || window.pageYOffset;
    const elementOffsetTop = el.offsetTop;
    const elementHeight = el.offsetHeight;

    // Calculate percentage of the element that's been seen
    const distance = scrollTop + viewportHeight - elementOffsetTop;
    let percentage = Math.round(distance / ((viewportHeight + elementHeight) / 100) - 12.5);

    // Restrict the range to between 0 and 100
    return Math.min(100, Math.max(0, percentage));
}

function marginToTop(el) {
    let prueba = false;
    let viewportHeight = window.innerHeight;
    let elHeight = el.getBoundingClientRect().top;

    //console.log(el, elHeight, viewportHeight);

    let top = viewportHeight * 0.015, bottom = viewportHeight * 0.35;

    if(elHeight > top && elHeight < bottom) {
        prueba = true;
    }

    return prueba;
}

export { isMobile, isSmallMobile, isDevice, isElementInViewport, isElementInViewportCards, percentageOfElement, marginToTop };